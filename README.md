这是一个使用C++编写的文本编辑器Notepad--,可以支持Win/Linux/Mac平台。
我们的目标是要替换Notepad++，重点在国产Uos系统、Mac 系统上发展。

一个支持windows/linux/mac的文本编辑器，目标是要替换notepad++，来自中国。

对比Notepad++而言，我们的优势是可以跨平台，支持linux mac操作系统。

 **鉴于Notepad++作者的错误言论，Notepad--的意义在于：减少一点错误言论，减少一点自以为是。** 

 **严正声明，台湾是中国的一部分。** 

您可以在这个项目提交bug或反馈问题。

最新版本下载地址：https://gitee.com/cxasm/notepad--/releases/tag/v1.16

最新开发版下载地址：https://gitee.com/cxasm/notepad--/releases/tag/v1.17

### 编译说明：

1）使用qtcreator 或 vs  先打开qscint/src/qscintilla.pro 。先编译出这个qscintlla的依赖库。

2）再打开RealCompare.pro 加载后编译。

3）由于编译的平台较多，涉及windows/linux/mac，有任何编译问题，还请加qq群 959439826 。欢迎广大网友实现新功能后提交代码给我们。

代码上线不久，删除了商业的对比功能和注册功能（这部分有商业原因，请理解），除此以外，所有功能全部保留。

![输入图片说明](20221107_160824.png)

![输入图片说明](1.png)

![输入图片说明](2.png)

![输入图片说明](https://foruda.gitee.com/images/1662043666536201252/709f7f20_2138353.png)